# General info #

Simple spring boot application providing rest service to find matching holidays for 2 different countries from specific date.

### How do I get set up? ###

Eeasiest options are - run as java application from IDE or install maven and execute "mvn spring-boot:run" in root directory.
You can read more at http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html